# A scalable approach for detecting collective Twitter Bots

## Autores
- Marco Patiño Lopez
- Armando Rodriguez Ramos

## Instrucciones
Nuestra primera recomendación es acceder a la carpeta de drive que contiene todos los archivos https://drive.google.com/drive/folders/1UY18iJbdNmKzEb1XQPGs7GqKExdWj2dJ?usp=sharing y utilizar los Notebooks con la plaraforma de Google Colab

En otro caso para instalar todas las librerias necesarias usar:

`pip install -r requirements.txt`

Una vez realizada esta acción ejecutar el `archivo.py` deseado tal que:

`python archivo.py`

Es indispensable disponer de los datasets en las carpetas correspondientes para poder ejecutar los archivos .py.

Aún así, esta practica se ha hecho usando Notebooks a través de la plataforma Google Colab. Por lo tanto será seguramente mejor utilizar los .ipynb que hemos adjuntado o la carpeta de drive comentada.

## Descripción contenido

README.md

requirements.txt

TwitterBots_MarcoPatino_ArmandoRodriguez.pdf

Datasets folder

- Link to download the dataset: https://botometer.osome.iu.edu/bot-repository/datasets/cresci-stock-2018/cresci-stock-2018.tar.gz
- Raw features dataset: cresci-stock-2018_tweets.json
- Raw target dataset: cresci-stock-2018.tsv
- Merged dataset: botNoBot2018.csv
- Preprocessed dataset: botNoBot2018_preprocessed.csv

Code folder
- Python
    - mergingdatasets.py
    - generalpreprocessing.py
    - Models folder
        - logisticregression.py
        - knn.py
        - linearsvm.py
        - mlp.py
        - rbfsvm.py
        - randomforest.py
- Notebook
    - MergingDatasets.ipynb
    - GeneralPreprocessing.ipynb
    - Models folder
        - LogisticRegression.ipynb
        - KNN.ipynb
        - linearSVM.ipynb
        - MLP.ipynb
        - rbfSVM.ipynb
        - randomForest.ipynb